//
//  ArrivalInfoCollectionviewCell.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 28/09/23.
//

import UIKit
import WebKit

class CommonCollectionviewCell: UICollectionViewCell, WKNavigationDelegate{
    
    @IBOutlet weak var webviewContent: UIView!
    @IBOutlet weak var collectionviewData: UICollectionView!
    var vmHotelCategory = HotelCategoryVM()
    var selectedIndex = 0
    
    var arrArrivalData = ["Getting Here","Parking","Free Authentication"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionviewSetup()
    
        
    }
    func getHotelCategory(hotelMenu_ID: String){
        vmHotelCategory.getHotelCategory(fiMenuId: hotelMenu_ID) {
            self.collectionviewData.reloadData()
        } onError: { error in
            print(error)
        }

    }
    func setTitle(){
        if selectedIndex == 0{
            
        }
    }
    func webviewData(){
        guard let url = URL(string: "")  else {return}
        
           let request = URLRequest(url: url)
           let webView = WKWebView(frame: self.webviewContent.frame)
           webView.autoresizingMask = [.flexibleWidth, .flexibleHeight] //It assigns Custom View height and width
           webView.navigationDelegate = self
           
           webView.load(request)
           self.webviewContent.addSubview(webView)
        
           self.webviewContent.layoutIfNeeded()
    }
    
    func collectionviewSetup(){
        collectionviewData.delegate = self
        collectionviewData.dataSource = self
        collectionviewData.register(UINib(nibName: "ArrivalDataCollectionviewCell", bundle: nil), forCellWithReuseIdentifier: "ArrivalDataCollectionviewCell")
    }
}
extension CommonCollectionviewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vmHotelCategory.categoryList?.count ?? 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionviewData.dequeueReusableCell(withReuseIdentifier: "ArrivalDataCollectionviewCell", for: indexPath) as! ArrivalDataCollectionviewCell
        cell.lblData.text = vmHotelCategory.categoryList?[indexPath.row].fsTitle ?? ""
         
            return cell
       
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 20
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let text = vmHotelCategory.categoryList?[indexPath.item].fsTitle ?? ""
               
               // Calculate the size needed for the cell based on the label's content
               let labelSize = (text as NSString).size(withAttributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]) // Adjust font and size as needed
               
               // You can add some padding or adjust the width as needed
               let cellWidth = labelSize.width + 30 // Adding 20 for padding
               
               return CGSize(width: cellWidth, height: collectionView.bounds.height)
           // return CGSize(width: (collectionviewData.frame.size.width/2) - 50, height: 50)

    }
    
}

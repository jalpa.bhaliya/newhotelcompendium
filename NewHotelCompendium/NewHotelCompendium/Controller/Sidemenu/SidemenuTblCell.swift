//
//  SidemenuTblCell.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 28/09/23.
//

import UIKit

class SidemenuTblCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

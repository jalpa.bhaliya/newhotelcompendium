//
//  GetHotelListModel.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 03/10/23.
//

import Foundation

struct HotelListRootClass : Codable {

    let fbIsSuccess : Bool?
    let foHotelList : [HotelListFoHotelList]?
    let foMenuList : [HotelListFoMenuList]?


    enum CodingKeys: String, CodingKey {
        case fbIsSuccess = "fbIsSuccess"
        case foHotelList = "foHotelList"
        case foMenuList = "foMenuList"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fbIsSuccess = try values.decodeIfPresent(Bool.self, forKey: .fbIsSuccess)
        foHotelList = try values.decodeIfPresent([HotelListFoHotelList].self, forKey: .foHotelList)
        foMenuList = try values.decodeIfPresent([HotelListFoMenuList].self, forKey: .foMenuList)
    }


}
struct HotelListFoMenuList : Codable {

    let fiMenuId : String?
    let fsImage : String?
    let fsMenuName : String?
    let sort : Int?


    enum CodingKeys: String, CodingKey {
        case fiMenuId = "fiMenuId"
        case fsImage = "fsImage"
        case fsMenuName = "fsMenuName"
        case sort = "sort"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fiMenuId = try values.decodeIfPresent(String.self, forKey: .fiMenuId)
        fsImage = try values.decodeIfPresent(String.self, forKey: .fsImage)
        fsMenuName = try values.decodeIfPresent(String.self, forKey: .fsMenuName)
        sort = try values.decodeIfPresent(Int.self, forKey: .sort)
    }


}
struct HotelListFoHotelList : Codable {

    let fiHotelsId : String?
    let fiLatitude : String?
    let fiLongitude : String?
    let fsAddress : String?
    let fsAppBackgroundImage : String?
    let fsHotelName : String?
    let fsImage : String?
    let fsShortDesc : String?


    enum CodingKeys: String, CodingKey {
        case fiHotelsId = "fiHotelsId"
        case fiLatitude = "fiLatitude"
        case fiLongitude = "fiLongitude"
        case fsAddress = "fsAddress"
        case fsAppBackgroundImage = "fsAppBackgroundImage"
        case fsHotelName = "fsHotelName"
        case fsImage = "fsImage"
        case fsShortDesc = "fsShortDesc"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fiHotelsId = try values.decodeIfPresent(String.self, forKey: .fiHotelsId)
        fiLatitude = try values.decodeIfPresent(String.self, forKey: .fiLatitude)
        fiLongitude = try values.decodeIfPresent(String.self, forKey: .fiLongitude)
        fsAddress = try values.decodeIfPresent(String.self, forKey: .fsAddress)
        fsAppBackgroundImage = try values.decodeIfPresent(String.self, forKey: .fsAppBackgroundImage)
        fsHotelName = try values.decodeIfPresent(String.self, forKey: .fsHotelName)
        fsImage = try values.decodeIfPresent(String.self, forKey: .fsImage)
        fsShortDesc = try values.decodeIfPresent(String.self, forKey: .fsShortDesc)
    }


}

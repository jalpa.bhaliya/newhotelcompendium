//
//  TextFieldFont.swift

import Foundation
import UIKit


extension UITextField {

    @IBInspectable
    public var isFontAutoSize: Bool{
        get {
            return self.font == UIFont(name: self.font!.fontName, size: self.font!.pointSize)
        }
        set {
            self.font = UIFont(name: self.font!.fontName, size: (UIScreen.main.bounds.size.width * self.font!.pointSize) / 320)
        }
    }
    
    
     @IBInspectable
     public var appFont: String{
         get {
             return  self.font!.fontName
         }
         set {
            self.font = UIFont(name: newValue, size: self.font!.pointSize)
         }
     }
    
}

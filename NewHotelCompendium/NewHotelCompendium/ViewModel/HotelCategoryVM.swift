//
//  HotelCategoryVM.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 04/10/23.
//

import Foundation
import KRProgressHUD
import Moya

class HotelCategoryVM{
  
    var categoryList : [HotelCategoryFoCatList]?
    var hotelMenu_id = ""
    //MARK: - Api for get dashboard Data
    func getHotelCategory(fiMenuId:String,onSuccess: @escaping () -> Void, onError: @escaping (String) -> Void) {
        KRProgressHUD.show()
      
        Network.request(.hotelCategory(fiMenuId: fiMenuId),decodeType: HotelCategoryRootClass.self, errorDecodeType: HotelCategoryRootClass.self, decoder: JSONDecoder(), success: { [weak self] isSuccess , data in
           
            KRProgressHUD.dismiss()
           
            if isSuccess == true {
                self?.categoryList = data.foCatList ?? []
                onSuccess()
            } else {
                onError(AppConfig.Alert.SomethingWrong)
            }
        },error:{ statusCode, data, message in
            KRProgressHUD.dismiss()
            onError(AppConfig.Alert.SomethingWrong)
        }, failure: { error in
            KRProgressHUD.dismiss()
            onError(error.localizedDescription)
        },completion: {
        })
    }
    
}

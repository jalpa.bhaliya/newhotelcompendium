//
//  HotelCategoriesCollectionviewCell.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 28/09/23.
//

import UIKit

class HotelCategoriesCollectionviewCell: UICollectionViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgHotelLogo: UIImageView!
    @IBOutlet weak var viewContainer: UIView!
  
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setCornerRadius(){
        viewContainer.borderWidth = 2
        viewContainer.borderColor = .white
    }
    func setDefault(){
        viewContainer.borderWidth = 0
        viewContainer.borderColor = .clear
    }
}

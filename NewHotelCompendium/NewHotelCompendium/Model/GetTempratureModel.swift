//
//  GetTempratureModel.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 04/10/23.
//

import Foundation

struct GetTempratureRootClass : Codable {

   
    let main : GetTempratureMain?
  

    enum CodingKeys: String, CodingKey {
      
        case main = "main"
      
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        main = try values.decodeIfPresent(GetTempratureMain.self, forKey: .main)
      
    }


}
struct GetTempratureMain : Codable {

    let feelsLike : Float?
    let humidity : Int?
    let pressure : Int?
    let temp : Float?
    let tempMax : Float?
    let tempMin : Float?


    enum CodingKeys: String, CodingKey {
        case feelsLike = "feels_like"
        case humidity = "humidity"
        case pressure = "pressure"
        case temp = "temp"
        case tempMax = "temp_max"
        case tempMin = "temp_min"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        feelsLike = try values.decodeIfPresent(Float.self, forKey: .feelsLike)
        humidity = try values.decodeIfPresent(Int.self, forKey: .humidity)
        pressure = try values.decodeIfPresent(Int.self, forKey: .pressure)
        temp = try values.decodeIfPresent(Float.self, forKey: .temp)
        tempMax = try values.decodeIfPresent(Float.self, forKey: .tempMax)
        tempMin = try values.decodeIfPresent(Float.self, forKey: .tempMin)
    }


}

//
//  HomeDetailCollectionviewCell.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 28/09/23.
//

import UIKit
import SDWebImage

class HomeDetailCollectionviewCell: UICollectionViewCell {

    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
   
   
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
   
    func configure(item:HotelListFoHotelList){
        lblAddress.text = item.fsAddress
        lblDescription.text = item.fsShortDesc
        imgHome.sd_setImage(with: URL(string: item.fsImage ?? ""), placeholderImage: UIImage(named: ""))
    }
   
    
}

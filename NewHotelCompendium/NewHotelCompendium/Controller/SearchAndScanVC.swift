//
//  SearchAndScanVC.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 29/09/23.
//

import UIKit
import AVFoundation
import AVKit

var hotelIdScn = ""
var isFisrtTimeCal = 0
var screenId = 0

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
    }
}
class SearchAndScanVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet weak var btnQr: UIButton!
    
    var session = AVCaptureSession()
    var video = AVCaptureVideoPreviewLayer()
    var objCaptureSession:AVCaptureSession?
    var captureDevice: AVCaptureDevice?
    var objCaptureVideoPreviewLayer:AVCaptureVideoPreviewLayer?
    var vwQRCode:UIView?
  
    override func viewDidLoad() {
        super.viewDidLoad()

        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            self.call_qr()
            
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                self.checkCamera()
            })
        }
    }

    @IBAction func btnScanQR(_ sender: UIButton) {
        if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
            // Already Authorized
            self.call_qr()
            
        } else {
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted: Bool) -> Void in
                self.checkCamera()
            })
        }
    }
    func configureVideoCapture() {
        let objCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        var error:NSError?
        let objCaptureDeviceInput: AnyObject!
        do {
            objCaptureDeviceInput = try AVCaptureDeviceInput(device: objCaptureDevice!) as AVCaptureDeviceInput
            
        } catch let error1 as NSError {
            error = error1
            objCaptureDeviceInput = nil
        }
        if (error != nil) {
            let alertView:UIAlertView = UIAlertView(title: "Device Error", message:"Device not Supported for this Application", delegate: nil, cancelButtonTitle: "Ok Done")
            alertView.show()
            return
        }
        objCaptureSession = AVCaptureSession()
        objCaptureSession?.addInput(objCaptureDeviceInput as! AVCaptureInput)
    
        let objCaptureMetadataOutput = AVCaptureMetadataOutput()
        objCaptureSession?.addOutput(objCaptureMetadataOutput)
        objCaptureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        objCaptureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
    }
    func addVideoPreviewLayer()
    {
        objCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: objCaptureSession!)
        objCaptureVideoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        objCaptureVideoPreviewLayer?.frame = view.layer.bounds
        self.view.isUserInteractionEnabled  = false
        self.view.layer.addSublayer(objCaptureVideoPreviewLayer!)
        objCaptureSession?.startRunning()
        //        self.view.bringSubviewToFront(lblQRCodeResult)
        //        self.view.bringSubviewToFront(lblQRCodeLabel)
    }
    func initializeQRView() {
        vwQRCode = UIView()
        vwQRCode?.layer.borderColor = UIColor.red.cgColor
        vwQRCode?.layer.borderWidth = 5
        vwQRCode?.isUserInteractionEnabled = false
        self.view.addSubview(vwQRCode!)
        self.view.bringSubviewToFront(vwQRCode!)
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        //dismiss(animated: true)
    }
    func found(code: String) {
        print(code)
        if(code  == "")
        {
            let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "GitftCardMsgAlertVC") as! GitftCardMsgAlertVC
            VC1.caseId = 1
            VC1.strMsg = "This property is not validate,please contact to support team."
            
            DispatchQueue.main.async {
                self.present(VC1, animated: true, completion:{
                    self.session.startRunning()
                })
            }
        }
        else
        {
            let newURL = URL(string: code)!
            if(newURL.valueOf("token") != nil)
            {
                
                
                session.stopRunning()
                isFisrtTimeCal = 0
                UserDefaults.standard.removeObject(forKey: "NotificationArray")
                UserDefaults.standard.synchronize()
                hotelIdScn = newURL.valueOf("token")!
                print(hotelIdScn)
                UserDefaults.standard.set(hotelIdScn, forKey: "fihotelId")
                apiCallForHotelAnalytics()
                UserDefaults.standard.synchronize()
                let vc:MyTabViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyTabViewController") as! MyTabViewController
                self.navigationController?.pushViewController(vc, animated: true)
                return
            }
            else{
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "GitftCardMsgAlertVC") as! GitftCardMsgAlertVC
                VC1.caseId = 1
                VC1.strMsg = "This property is not validate,please contact to support team."
                
                DispatchQueue.main.async {
                    self.present(VC1, animated: true, completion:{})
                }
                return
            }
        }
    }
    func apiCallForHotelAnalytics(){
        print("Call Api for hotel list")
    }
    func call_qr()
    {
        // let session = AVCaptureSession()
        if(!session.isRunning)
        {
            let captureDevice = AVCaptureDevice.default(for: .video)
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice!)
                session.addInput(input)
            } catch {
                return
                //  self.view.makeToast("Sorry Scanner is not working!")
            }
            
            let output = AVCaptureMetadataOutput()
            session.addOutput(output)
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            video = AVCaptureVideoPreviewLayer(session: session)
            //video.frame = view.layer.bounds
            video.frame = btnQr.frame
            video.cornerRadius = 10
            video.videoGravity = .resizeAspectFill
            view.layer.addSublayer(video)
            session.startRunning()
            
        }
    }
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
        switch authStatus {
        case .authorized: break // Do your stuff here i.e. allowScanning()
        case .denied: alertToEncourageCameraAccessInitially()
        case .notDetermined: alertPromptToAllowCameraAccessViaSetting()
        default: alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (alert) -> Void in
            UIApplication.shared.openURL(URL(string: UIApplication.openSettingsURLString)!)
        }))
        present(alert, animated: true, completion: nil)
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        let alert = UIAlertController(
            title: "Warning",
            message: "Camera access required for QR Scanning",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (alert) -> Void in
            if AVCaptureDevice.devices(for: AVMediaType.video).count > 0 {
                AVCaptureDevice.requestAccess(for: AVMediaType.video) { granted in
                    DispatchQueue.main.async() {
                        self.checkCamera() } }
            }
        }))
        present(alert, animated: true, completion: nil)
        
        
    }
    
}

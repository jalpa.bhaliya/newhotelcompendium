//
//  Helper.swift

import Foundation
import UIKit
import AVKit
import SDWebImage


//APPLICATION  CONSTANSTS
let appName = Bundle.appName()
let directoryName : String = "Spyne-\(Date().toString(format: "ddMMyyyyHHmmss"))"
let POSTHOG_API_KEY = "FoIzpWdbY_I9T_4jr5k4zzNuVJPcpzs_mIpO6y7581M"

class Alert{
    
    //NetWork Messages
    static let Network = "Oops! Please Check Data Connection".localized()
    static let Timeout = "Oops! Poor Data Connection".localized()
    static let Unknown = "It is unknown whether the network is reachable".localized()
    static let SomethingWrong = "Somethong went wrong"
    static let success = "success"
    static let emailAllreadyExist = "email allready exist."
    
    
    //Data Validation
    static let EmptyGender = "Please select gender"
    static let emptyHeight = "please select your height"
    static let emptyFoodPreference = "Please select at least one Food Preference!"
    static let emptyHowToActive = "Please let us know 'How active are you?'"
    static let emptyFitnessLevel = "Please let us know 'Your Fitness Level?'"
    static let emptyHowToActiveAndFitnessLevel = "Please let us know your 'Activity & Fitness Level'"
    static let emptyWeight = "please select your weight"
    static let emptyWhatisBring = "Please let us know 'What brings you to Fitcru?'"
    class Register{
        static let invalidOTP = "Please enter a valid OTP!"
        static let EmptyMobile = "Please enter the Mobile Number registered with us!"
        static let InvalidMobile = "Please enter a valid Mobile Number!"
        static let EmptyEmail = "Please enter the Email registered with us!"
        static let InvalidEmail = " Please enter a valid Email!"
        static let EmptyPassword = "Please enter the password!"
    }
    class Login{
        static let invalidOTP = "Please enter a valid OTP!"
        static let EmptyMobile = "Please enter the Mobile Number!"
        static let InvalidMobile = "Please enter a valid Mobile Number!"
        static let EmptyEmail = "Please enter the Email!"
        static let EmptyEmailOrMobile = "Please enter the Email or Mobile Number!"
        static let InvalidEmail = " Please enter a valid Email!"
        static let EmptyPassword = "Please enter the password!"
    }
    static let InvalidPincode = "Please enter valid pincode".localized()
    
    static let EmptyConfirmPassword = "Please enter confirm password".localized()
    static let InvalidPassword = "Password must be contains atlest one uppercase, lowercase and numeric value and minimum lenth 8"//"Please enter proper password, Minimum of 8 characters, 1 uppercase letter,  1 lowercase letter, 1 numeber "
    static let InvalidPasswordWithChar = "Password must be contains atlest one uppercase, lowercase and numeric value".localized()
    static let InvalidConfirmPass = "Confirm Password not match".localized()
    static let EmptyName = "Please enter name!"
    static let EmptyFirstName = "Please enter your First Name! "
    static let EmptyLastName = "Please enter your Last Name!"
    static let EmptyAddress = "Please enter address".localized()
    static let EmptyCity = "Please enter city".localized()
    static let EmptyState = "Please enter state".localized()
    static let EmptyZipcode = "Please enter pincode".localized()
    static let EmptyContact = "Please enter contact number".localized()
    static let EmptyEducation = "Please select education".localized()
    static let EmptySubject = "Please select Subject".localized()
    static let EmptyRating = "Please add rating".localized()
    static let EmptyDescription = "Please enter description".localized()
    static let EmptyTopicName = "Empty topic name".localized()
    static let EmptyType = "Select Type".localized()
    static let EmptyCountryName = "Please Select Country".localized()
    
    static let EmptyBabyName = "Please enter baby name".localized()
    static let EmptyBabyBirthDate = "Please select baby birth date".localized()
    static let EmptyBabyBirthTime = "Please select baby birth time".localized()
    static let EmptyBabyBirthWeight = "Please enter baby weight".localized()
    static let EmptyBabyBirthHeight = "Please enter baby height".localized()
    static let EmptyBabyGender = "Please select baby gender".localized()
    
    static let EmptyDate = "We would like to wish you a 'Happy Birthday' Please provide your Date of Birth!"
    static let EmptyTime = "Please select time".localized()
    static let EmptyColor = "Please select color".localized()
    static let EmptyTexture = "Please select texture".localized()
    
    static let EmptyOTP = "Please Enter OTP".localized()
    static let InvalidOTPLenth = "OTP Must be 6 Digit" .localized()
    static let ImagesDownloaded = "Image Download Completed!"
    static let fistNameAllowOnlyAlphabetic = "First name should contain Alphabets only!"
    static let lastNameAllowOnlyAlphabetic = "Last name should contains Alphabets only!" //
   // static let AllowOnlyAlphabetic = "please enter only alphabetic value in"
    

    static let emptyGoal = "Please select a goal"
    static let emptyShortTermWeek = "Please enter short term week value"
    static let emptyShortTermWeight = "Please enter short term weight value"
    static let emptyLongTermWeek = "Please enter long term week value"
    static let emptyLongTermWeight = "Please enter long term weight value"
    static let alReadySkipMeal = "You have already skip this meal"
    
    static let emptyCurrentPassword = "Please enter current password"
    static let emptyNewPassword = "Please enter new password"
    static let emptyConfirmPassword = "Please enter confirm password"
    static let notMatchNewPasswordAndConfirmPassword = "Confirm Password not match"
    static let invalidCurrentPassword = "Current password must be contains atlest one uppercase, lowercase, numeric value and minimum lenth 8"
    static let invalidNewPassword = "New Password must be contains atlest one uppercase, lowercase, numeric value and minimum lenth 8"
    static let invalidConfirmPassword = "Confirm Password must be contains atlest one uppercase, lowercase, numeric value and minimum lenth 8"

}
class StringCons{
    static let deviceName:String = "IOS"
    class SocialType{
        static let facebook:String = "Facebook"
        static let google:String = "Google"
        static let apple:String = "Apple"
        
    }
    class ResendOTP{
        static let firstPart:String = "Please enter OTP received on your mobile, \n Didn't receive the OTP?"
        static let resend:String = " Resend"
    }
    class Workout{
        static let navigationTitle:String = "Workouts"
        static let workoutTabItems : [String] = ["Plan","Studio","Challenges","Today's Session"]
    }
    static let planDetail = " Plan Details"
    static let foodOrder = " Food Order"
    static let cart = " My Cart"
    static let myOrder = " My Order"
    static let myProfile = " My Profile"
    static let hello = "Hello"
    static let congratulations = "Congratulations!"
    static let youHaveCompletedOurQuestionnaire = "You have completed our Questionnaire.\n"
    static let weThankYouforYourPatience = "We Thank You for Your Patience!"
    static let workoutDetail = " Workout Name"
    static let startingQuotes:String = "\u{275D}"
    static let endingQuotes:String = "\u{275E}"
    static let navigationLeftBarButtonTitleSize:CGFloat = 19
    static let totalPercentage:Float = 100
    static let personalInformation:String = " Personal Information"
    static let helthInformation:String = " Helth Information"
    static let maxRatingValue:Int = 10
    static let changePassword:String = " Change Password"
    static let minutes:String = "minutes"
    static let nutritionTracking:String = "Nutrition Tracking" //Nutrition Tracking
    static let orderWeeklyMeals:String = "Order Weekly Meals"
    static let orderId:String = "Order ID"
    static let setYourAvailablity:String = "Set your availability for "
   
}

/* MARK:- ====   MARK: RELOAD VIEW CONTROLLER  ==== */
extension UIViewController {
    func reloadViewFromNib() {
        let parent = view.superview
        view.removeFromSuperview()
        view = nil
        parent?.addSubview(view) // This line causes the view to be reloaded
    }
    
    func globalAlert(msg: String) {
        
        let alertView:UIAlertController = UIAlertController(title:  appName, message: msg, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            alertView.dismiss(animated: true, completion: nil)
        }))
        // alertView.show()
        self.present(alertView, animated: true, completion: nil)
    }
    
    
    func globalAlertWithOption(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func setNavigationBarTintColor()  {
        self.navigationController?.navigationBar.barTintColor = .white
    }
    
//    
//    func posthogIdentify(identity:String,properties:[String:String]){
//        guard  let posthog = PHGPostHog.shared() else {return}
//        var property = properties
//        property.add(["app_name":appName])
//        posthog.identify(identity,properties: property)
//        
//    }
//    
//    
//    func posthogCapture(identity:String,properties:[String:String]){
//        guard  let posthog = PHGPostHog.shared() else {return}
//        var property = properties
//        property.add(["app_name":appName])
//        posthog.identify(identity,properties: property)
//        
//    }
//    
//    func posthogLogout(){
//        // in swift
//        guard  let posthog = PHGPostHog.shared() else {return}
//        posthog.capture("Logged Out")
//        posthog.flush()
//    }
//    
    
}


enum Screen: String {
    case home = "home"
    case camera = "camera"
}


func randomString(length: Int) -> String {
  let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  return String((0..<length).map{ _ in letters.randomElement()! })
}


//
//  SidemenuVC.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 28/09/23.
//

import UIKit

class SidemenuVC: UIViewController {

    @IBOutlet weak var tblSidemenu: UITableView!
    var arrTitle = ["Compodiam Home","Search Hotel Facilities","Arrival Information","Dinning Information","Location Information","Open in Google Map","My Notifications","Search Compodiam","Book This Compodiam","Download PDF","Share with friends"]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
       setUpSidemenu()
    }
    
    func setUpSidemenu(){
        tblSidemenu.delegate = self
        tblSidemenu.dataSource = self
        tblSidemenu.register(UINib(nibName: "SidemenuTblCell", bundle: nil), forCellReuseIdentifier: "SidemenuTblCell")
    }
}

extension SidemenuVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSidemenu.dequeueReusableCell(withIdentifier: "SidemenuTblCell", for: indexPath) as! SidemenuTblCell
        cell.lblTitle.text = arrTitle[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

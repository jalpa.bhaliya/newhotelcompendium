//
//  HotelCategoryModel.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 04/10/23.
//

import Foundation

struct HotelCategoryRootClass : Codable {

    let fbIsSuccess : Bool?
    let foCatList : [HotelCategoryFoCatList]?


    enum CodingKeys: String, CodingKey {
        case fbIsSuccess = "fbIsSuccess"
        case foCatList = "foCatList"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fbIsSuccess = try values.decodeIfPresent(Bool.self, forKey: .fbIsSuccess)
        foCatList = try values.decodeIfPresent([HotelCategoryFoCatList].self, forKey: .foCatList)
    }


}
struct HotelCategoryFoCatList : Codable {

    let fiCatId : String?
    let fiCount : String?
    let fiMenuId : String?
    let fsImage : String?
    let fsTitle : String?


    enum CodingKeys: String, CodingKey {
        case fiCatId = "fiCatId"
        case fiCount = "fiCount"
        case fiMenuId = "fiMenuId"
        case fsImage = "fsImage"
        case fsTitle = "fsTitle"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        fiCatId = try values.decodeIfPresent(String.self, forKey: .fiCatId)
        fiCount = try values.decodeIfPresent(String.self, forKey: .fiCount)
        fiMenuId = try values.decodeIfPresent(String.self, forKey: .fiMenuId)
        fsImage = try values.decodeIfPresent(String.self, forKey: .fsImage)
        fsTitle = try values.decodeIfPresent(String.self, forKey: .fsTitle)
    }


}

//
//  TextFieldFont.swift

import Foundation
import UIKit

extension UIButton {

    @IBInspectable
    public var isFontAutoSize: Bool{
        get {
             return self.titleLabel!.font == UIFont(name: self.titleLabel!.font!.fontName, size: self.titleLabel!.font!.pointSize)
        }
        set {
             self.titleLabel!.font = UIFont(name: self.titleLabel!.font!.fontName, size:  (UIScreen.main.bounds.size.width * self.titleLabel!.font!.pointSize) / 320)
        }
    }
    
    @IBInspectable
       public var appFont: String{
           get {
            return (self.titleLabel?.font.fontName)!
           }
          set {
            self.titleLabel!.font = UIFont(name: newValue, size: self.titleLabel!.font.pointSize)!
           }
       }
    
    
    @IBInspectable
    public var isAppColor: Bool{
        get {
            return self.backgroundColor == UIColor.appColor
        }
        set {
            self.backgroundColor = UIColor.appColor
            self.setTitleColor(.white, for: .normal)
            
        }
    }
    
    
}

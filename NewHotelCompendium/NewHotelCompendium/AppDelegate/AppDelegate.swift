//
//  AppDelegate.swift
//
//  Created by Rajesh Shiyal.
//

import UIKit
import IQKeyboardManagerSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = "Done"
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

//MARK: - Appdelegate Navigation
extension AppDelegate{
//    static func navToSpalashScreen(){
//        let story = UIStoryboard(name: "Auth", bundle:nil)
//        let vc = story.instantiateViewController(withIdentifier: "AuthNavigationVC")
//        UIApplication.shared.windows.first?.rootViewController = vc
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
//    }
//    static func navToDashboard(){
//        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Dashboard", bundle: nil)
//
//        let leftMenuVC: FTCLeftMenuVC = mainStoryboard.instantiateViewController(withIdentifier: "FTCLeftMenuVC") as! FTCLeftMenuVC
//
//        let centerVC = mainStoryboard.instantiateViewController(withIdentifier: "CustomTabViewController") as! CustomTabViewController
//        let centerNavVC = UINavigationController(rootViewController: centerVC)
//
//        let rootController = FAPanelController()
//        rootController.configs.leftPanelWidth = 280
//        rootController.configs.bounceOnLeftPanelOpen = true
//        rootController.center(centerNavVC).left(leftMenuVC)
//        rootController.leftPanelPosition = .back
//
//        UIApplication.shared.windows.first?.rootViewController = rootController
//
//    }
////    static func navToDashboard(){
////        let story = UIStoryboard(name: "Dashboard", bundle:nil)
////        let vc = story.instantiateViewController(withIdentifier: "FTCTabbarVC")
////        UIApplication.shared.windows.first?.rootViewController = vc
////        UIApplication.shared.windows.first?.makeKeyAndVisible()
////    }
//    static func navToWorkout(){
//        let story = UIStoryboard(name: "Workout", bundle:nil)
//        let vc = story.instantiateViewController(withIdentifier: "FTCWorkoutCarbonkitVC")
//        UIApplication.shared.windows.first?.rootViewController = vc
//        UIApplication.shared.windows.first?.makeKeyAndVisible()
//    }
}



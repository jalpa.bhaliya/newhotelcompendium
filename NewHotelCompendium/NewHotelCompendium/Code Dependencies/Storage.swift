//
//  Storage.swift
//

import Foundation
class Storage{

    static let shared = Storage()
    private let defaults = UserDefaults.standard

//    func setProductCategories( CategoryData categories :CategoryData){
//        let encoder = JSONEncoder()
//        if let encoded = try? encoder.encode(categories) {
//            defaults.set(encoded, forKey: "categories")
//        }
//    }
//
//    func getProductCategories() -> CategoryData?{
//        if let categoriesObj = defaults.object(forKey: "categories") as? Data {
//            let decoder = JSONDecoder()
//            if let categories = try? decoder.decode(CategoryData.self, from: categoriesObj) {
//                return categories
//            }
//        }
//        return nil
//    }
//
//
//    func getUser() -> LoginRootClass?{
//        if let user = defaults.object(forKey: "User") as? Data {
//            let decoder = JSONDecoder()
//            if let user = try? decoder.decode(LoginRootClass.self, from: user) {
//                return user
//            }
//        }
//        return nil
//    }
//    func getCredit() -> UserCreditData?{
//        if let creditObj = defaults.object(forKey: "credit") as? Data {
//            let decoder = JSONDecoder()
//            if let credit = try? decoder.decode(UserCreditData.self, from: creditObj) {
//                return credit
//            }
//        }
//        return nil
//    }
//
    func clearAllStorage(){
        defaults.removeObject(forKey: "User")
    }
//}
//
//
//struct USER{
//
//    static var enterpriseId : String{
//        get{
//            if UserDefaults.standard.value(forKey: "enterpriseId") != nil{
//                return (UserDefaults.standard.value(forKey: "enterpriseId") as? String)!
//            }
//            return ""
//        }
//        set{
//            UserDefaults.standard.set(newValue, forKey: "enterpriseId")
//        }
//    }
//
//    var notificationToken : String{
//        get{
//            if UserDefaults.standard.value(forKey: "notificationToken") != nil{
//                return (UserDefaults.standard.value(forKey: "notificationToken") as? String)!
//            }
//            return ""
//        }
//        set{
//            UserDefaults.standard.set(newValue, forKey: "notificationToken")
//        }
//
//    }
//
//    var currentLang : String{
//        get{
//            if UserDefaults.standard.value(forKey: "currentLang") != nil{
//                return (UserDefaults.standard.value(forKey: "currentLang") as? String)!
//            }
//            return "en"
//        }
//        set{
//            UserDefaults.standard.set(newValue, forKey: "currentLang")
//        }
//
//    }
//
//    static var authKey : String{
//        get{
//            if UserDefaults.standard.value(forKey: "authKey") != nil{
//                return (UserDefaults.standard.value(forKey: "authKey") as? String)!
//            }
//            return ""
//        }
//        set{
//            UserDefaults.standard.set(newValue, forKey: "authKey")
//        }
//
//    }
//
//    static var userId : String{
//        get{
//            if UserDefaults.standard.value(forKey: "userId") != nil{
//                return (UserDefaults.standard.value(forKey: "userId") as? String)!
//            }
//            return ""
//        }
//        set{
//            UserDefaults.standard.set(newValue, forKey: "userId")
//        }
//
//    }
//

//    static var tokenId : String{
//        get{
//            if UserDefaults.standard.value(forKey: "tokenId") != nil{
//                return (UserDefaults.standard.value(forKey: "tokenId") as? String)!
//            }
//            return ""
//        }
//        set{
//            UserDefaults.standard.set(newValue, forKey: "tokenId")
//        }
//
//    }
//
//    static var userEmail : String{
//        get{
//            if UserDefaults.standard.value(forKey: "userEmail") != nil{
//                return (UserDefaults.standard.value(forKey: "userEmail") as? String)!
//            }
//            return ""
//        }
//        set{
//            UserDefaults.standard.set(newValue, forKey: "userEmail")
//        }
//
//    }
//
    static var isLogin : Bool{
        get{
            if UserDefaults.standard.value(forKey: "isLogin") != nil{
                return (UserDefaults.standard.value(forKey: "isLogin") as? Bool)!
            }
            return false
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isLogin")
        }

    }

    static var isFirstTime : Bool{
        get{
            if UserDefaults.standard.value(forKey: "isFirstTime") != nil{
                return (UserDefaults.standard.value(forKey: "isFirstTime") as? Bool)!
            }
            return true
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isFirstTime")
        }

    }

    static var isFirstTimeHome : Bool{
        get{
            if UserDefaults.standard.value(forKey: "isFirstTimeHome") != nil{
                return (UserDefaults.standard.value(forKey: "isFirstTimeHome") as? Bool)!
            }
            return true
        }
        set{
            UserDefaults.standard.set(newValue, forKey: "isFirstTimeHome")
        }

    }

}

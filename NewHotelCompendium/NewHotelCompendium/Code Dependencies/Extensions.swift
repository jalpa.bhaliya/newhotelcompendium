//
//  Extensions.swift
//  Copyright © 2021 WD. All rights reserved.
//

import Foundation
import UIKit
//MARK:- Extensions

extension UIColor{
    
    static let appColor = UIColor(hexString: "#000000")
    static let appSecondryColor = UIColor(hexString: "#FFFFFF")
    static let appTCAttributedColor = UIColor(hexString: "#6B6B6B")//6B6B6B
    static let appLightOrange = UIColor(hexString: "#F69C4C")
    //Static Colors
    static let appGreenColor = UIColor(hexString: "#47CF5D")
    static let pickerLableColor = UIColor(hexString: "#B7B7B7")
    static let appBlue = UIColor(hexString: "#4A80F0")
    static let appYellow = UIColor(hexString: "#D7D26E")
    static let appGreen = UIColor(hexString: "#65C774")
    static let appLogoColor = UIColor(hexString: "#01D3E2")
    static let appLightGray = UIColor(hexString: "#AAAAAA")
    static let appBlack = UIColor(hexString: "#000000")
    static let appTableBackgroundColor = UIColor(hexString: "#EDEDED")
    static let nutritionChartProgressColor = UIColor(hexString: "#1DB20D")
    static let nutritionChartProgressbackgroungColor = UIColor(hexString: "#38383D")
    static let nutritionDetailChartProgressColor = UIColor(hexString: "#06E9FD")
    static let nutritionDetailChartProgressbackgroungColor = UIColor(hexString: "#38383D")
    static let weeklyMealUnselectBackgroundColor = UIColor(hexString: "#F4F4F4")
    static let myOrderProccesingColor = UIColor(hexString: "#EFB000")
    static let myOrderDeliveredColor = UIColor(hexString: "#0EBF04")
    static let myOrderCancelledColor = UIColor(hexString: "#ED2E0F")
    static let lightGrayLableColor = UIColor(hexString: "#757575")
    //
    var hexString:String? {
        if let components = self.cgColor.components {
            if components.count > 2{
                let r = components[0]
                let g = components[1]
                let b = components[2]
                return  String(format: "%02X%02X%02X", (Int)(r * 255), (Int)(g * 255), (Int)(b * 255))
            }
            return nil
        }
        return nil
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension Bundle {
    static func appName() -> String {
        guard let dictionary = Bundle.main.infoDictionary else {
            return ""
        }
        if let version : String = dictionary["CFBundleName"] as? String {
            return version
        } else {
            return ""
        }
    }
}

extension UIImage {
    convenience init(view: UIView) {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: false)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
        
    }
}

extension UIImage {
    func overlayWith(image: UIImage, posX: CGFloat, posY: CGFloat) -> UIImage {
        let newWidth = posX < 0 ? abs(posX) + max(self.size.width, image.size.width) :
            size.width < posX + image.size.width ? posX + image.size.width : size.width
        let newHeight = posY < 0 ? abs(posY) + max(size.height, image.size.height) :
            size.height < posY + image.size.height ? posY + image.size.height : size.height
        let newSize = CGSize(width: newWidth, height: newHeight)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        let originalPoint = CGPoint(x: posX < 0 ? abs(posX) : 0, y: posY < 0 ? abs(posY) : 0)
        self.draw(in: CGRect(origin: originalPoint, size: self.size))
        let overLayPoint = CGPoint(x: posX < 0 ? 0 : posX, y: posY < 0 ? 0 : posY)
        image.draw(in: CGRect(origin: overLayPoint, size: image.size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return newImage
    }
}



extension UIView {

    func takeSnapshotOfView() -> UIImage? {

        let size = CGSize(width: frame.size.width, height: frame.size.height)
        let rect = CGRect.init(origin: .init(x: 0, y: 0), size: frame.size)

        UIGraphicsBeginImageContext(size)
        drawHierarchy(in: rect, afterScreenUpdates: true)

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        guard let imageData = image?.pngData() else {
           return nil
        }

        return UIImage.init(data: imageData)
    }
    
    func addshadow(top: Bool,
                       left: Bool,
                       bottom: Bool,
                       right: Bool,
                       shadowRadius: CGFloat = 2.0,
                        shadowColor: UIColor = UIColor.lightGray,
                   opacity: Float = 1.0
                        ) {

            self.layer.masksToBounds = false
            self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            self.layer.shadowRadius = shadowRadius
            self.layer.shadowColor = shadowColor.cgColor
            self.layer.shadowOpacity = opacity

            let path = UIBezierPath()
            var x: CGFloat = 0
            var y: CGFloat = 0
            var viewWidth = self.frame.width
            var viewHeight = self.frame.height

            // here x, y, viewWidth, and viewHeight can be changed in
            // order to play around with the shadow paths.
            if (!top) {
                y+=(shadowRadius+1)
            }
            if (!bottom) {
                viewHeight-=(shadowRadius+1)
            }
            if (!left) {
                x+=(shadowRadius+1)
            }
            if (!right) {
                viewWidth-=(shadowRadius+1)
            }
            // selecting top most point
            path.move(to: CGPoint(x: x, y: y))
            // Move to the Bottom Left Corner, this will cover left edges
            /*
             |☐
             */
            path.addLine(to: CGPoint(x: x, y: viewHeight))
            // Move to the Bottom Right Corner, this will cover bottom edge
            /*
             ☐
             -
             */
            path.addLine(to: CGPoint(x: viewWidth, y: viewHeight))
            // Move to the Top Right Corner, this will cover right edge
            /*
             ☐|
             */
            path.addLine(to: CGPoint(x: viewWidth, y: y))
            // Move back to the initial point, this will cover the top edge
            /*
             _
             ☐
             */
            path.close()
            self.layer.shadowPath = path.cgPath
    }

}

extension UINavigationController {
    func popToViewController(ofClass: AnyClass, animated: Bool = true) {
        if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
            popToViewController(vc, animated: animated)
        }
    }
}

extension UIImage {
    
    func saveToPhotoLibrary(_ completionTarget: Any?, _ completionSelector: Selector?) {
        DispatchQueue.global(qos: .userInitiated).async {
            UIImageWriteToSavedPhotosAlbum(self, completionTarget, completionSelector, nil)
        }
    }
    
}

extension UIAlertController {
    
    func present() {
        guard let controller = UIApplication.shared.windows.filter({$0.isKeyWindow}).first?.rootViewController else {
            return
        }
        controller.present(self, animated: true)
    }
}



extension Dictionary {
    var jsonStringRepresentation: String? {
        guard let theJSONData = try? JSONSerialization.data(withJSONObject: self,
                                                            options: [.prettyPrinted]) else {
            return nil
        }
        
        return String(data: theJSONData, encoding: .ascii)
    }
}




extension UIFont{

    public static func futuraCondensedExtraBold(witSize:CGFloat)->UIFont{
        return UIFont(name: "Futura Condensed ExtraBold", size: witSize)!
    }
    public static func futurMedium(witSize:CGFloat)->UIFont{
        return UIFont(name: "Futura Medium", size: witSize)!
    }
    public static func futuraBTLight(witSize:CGFloat)->UIFont{
        return UIFont(name: "FuturaBT-Light", size: witSize)!
    }
    public static func futuraPtMedium(witSize:CGFloat)->UIFont{
        return UIFont(name: "FuturaPT-Medium", size: witSize)!
    }
    public static func futuraPtBook(witSize:CGFloat)->UIFont{
        return UIFont(name: "FuturaPT-Book", size: witSize)!
    }
    public static func futuraPtDemiBold(witSize:CGFloat)->UIFont{
        return UIFont(name: "FuturaPT-DemiBold", size: witSize)!
    }
    public static func futuraPTLight(witSize:CGFloat)->UIFont{
        return UIFont(name: "FuturaPT-Light", size: witSize)!
    }
    //FuturaPT-DemiBold
}
extension UIButton {
    @IBInspectable var localizedTitle: String {
        get { return "" }
        set {
            self.setTitle(newValue.localized(), for: .normal)
        }
    }
}

extension UILabel {
    @IBInspectable var localizedTitle: String {
        get { return "" }
        set {
            self.text = newValue.localized()
        }
    }
}

extension UITextField {
    @IBInspectable var localizedTitle: String {
        get { return "" }
        set {
            self.text = newValue.localized()
        }
    }
    
    @IBInspectable var localizedPlaceHolder: String {
        get { return "" }
        set {
            self.placeholder = newValue.localized()
        }
    }
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.gray.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
    
}

extension UITextView {
    @IBInspectable var localizedTitle: String {
        get { return "" }
        set {
            self.text = newValue.localized()
        }
    }
}

extension Bundle {
    private static var bundle: Bundle!
    
    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            let appLang = UserDefaults.standard.string(forKey: "app_lang") ?? "en"
            let path = Bundle.main.path(forResource: appLang, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
        
        return bundle;
    }
    public static func setLanguage(lang: String) {
        UserDefaults.standard.set(lang, forKey: "app_lang")
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
}

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }

    func localizeWithFormat(arguments: CVarArg...) -> String{
        return String(format: self.localized(), arguments: arguments)
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
            let seconds: Int = totalSeconds % 60
            let minutes: Int = (totalSeconds / 60) % 60
            return String(format: "%02d:%02d", minutes, seconds)
    }
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    func html2Attributed() -> NSAttributedString? {
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    func htmlAttributed(using font: UIFont, hexaCode: String) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(font.pointSize)pt !important;" +
                "color: #\(hexaCode ) !important;" +
                "font-family: \(font.familyName), Helvetica !important;" +
                "}," +
                "</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    func htmlAttributed(using font: UIFont, color: UIColor) -> NSAttributedString? {
        do {
            
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(font.pointSize)pt !important;" +
                "color: \(color.toHexString()) !important;" +
                "font-family: \(font.familyName), Helvetica !important;" +
                "}" +
                "</style> \(self)"
       
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    func toDateString( inputDateFormat inputFormat  : String,  ouputDateFormat outputFormat  : String ) -> String
       {
           let dateFormatter = DateFormatter()
           dateFormatter.dateFormat = inputFormat
           let date = dateFormatter.date(from: self)
           dateFormatter.dateFormat = outputFormat
           return dateFormatter.string(from: date!)
       }

}


extension Int{
    
    func toMinSec()->String{
        let seconds: Int = self % 60
        let minutes: Int = (self / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }

}

extension UINavigationController {

    override open var shouldAutorotate: Bool {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.shouldAutorotate
            }
            return super.shouldAutorotate
        }
        
        
    }

    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.preferredInterfaceOrientationForPresentation
            }
            return super.preferredInterfaceOrientationForPresentation
        }
    }

    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.supportedInterfaceOrientations
            }
            return super.supportedInterfaceOrientations
        }
    }

}


extension Float {
    func round(to places: Int) -> Float {
        let divisor = pow(10.0, Float(places))
        return Darwin.round(self * divisor) / divisor
    }
}

extension Double {
    func round(to places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return Darwin.round(self * divisor) / divisor
    }
}


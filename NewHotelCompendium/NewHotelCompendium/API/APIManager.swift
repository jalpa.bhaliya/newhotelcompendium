
import Foundation
import Moya
import Alamofire
import UIKit

//let api_key = "ee84dc3e-340e-4d39-a24c-9365272405f9"
//let strategy = "PASSWORD"
var mainURL = "https://quest.hotelcompendium.com.au/api/"
enum APIManager {
    case getHotelName(fiHotelsId:String)
    case getTemprature(lat:String,lon:String,appid:String,units:String)
    case hotelCategory(fiMenuId:String)
}

extension APIManager: TargetType  {
    
    
    var headers: [String: String]? {
        
        switch self {
//        case .login:
//            let headerParam = [
//                "Accept": "application/json",
//                "Authorization": "Bearer \(User.token)"
//            ]
//            return headerParam
//            
        default:
            let headerParam = [
                "Accept": "application/json",
            ]
            return headerParam
        }
    }
    
    var baseURL: URL {
        switch self {
        case.getHotelName:
            return URL(string:mainURL)!
        case .getTemprature:
            return URL(string: "https://api.openweathermap.org/data/2.5/weather")!
        case .hotelCategory:
            return URL(string:mainURL)!
        default:
            return URL(string:"https://quest.hotelcompendium.com.au/api/")!
        }
    }
    
    var path: String
    {
        switch self {
        
        case .getHotelName:
            return "getHotelMenu"
        
        case .getTemprature:
            return ""
        case .hotelCategory:
            return "getCategory"
        }
        
    }
    
    var method: Moya.Method {
        switch self {
            
//        case .getHotelName:
//            return .get
        case .getTemprature,.hotelCategory:
            return .get
        default:
            return .post
        }
    }
    
    
    var parameters: [String: Any]? {
        
        switch self {
        case .getHotelName(let fiHotelsId):
            return ["fiHotelsId":fiHotelsId]
        case .getTemprature(let lat, let lon, let appid, let units):
            return ["lat":lat,"lon":lon,"appid":appid,"units":units]
        case .hotelCategory(let fiMenuId):
            return ["fiMenuId":fiMenuId]
        default:
            return  nil
        }
    }
    
    var parameterEncoding: ParameterEncoding {
        switch self {
        case .getTemprature,.hotelCategory:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    var sampleData: Data {
        return "".utf8Encoded
    }
    
    var task: Task {
        switch self {
            
        default:
            if parameters != nil {
                return .requestParameters(parameters: parameters!, encoding: parameterEncoding)
            } else {
                return .requestPlain
            }
        }
    }
}

extension String {
    var utf8Encoded: Data { return data(using: .utf8)! }
}

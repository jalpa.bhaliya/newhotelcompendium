//
//  GetTempVM.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 04/10/23.
//

import Foundation
import KRProgressHUD
import Moya

class TempVM{
  
    var temprature : Float?
    //MARK: - Api for get dashboard Data
    func getTemprature(lat:String,lon:String,appid:String,units:String,onSuccess: @escaping () -> Void, onError: @escaping (String) -> Void) {
        KRProgressHUD.show()
      
        Network.request(.getTemprature(lat: lat, lon: lon, appid: appid, units: units),decodeType: GetTempratureRootClass.self, errorDecodeType: GetTempratureRootClass.self, decoder: JSONDecoder(), success: { [weak self] isSuccess , data in
           
            KRProgressHUD.dismiss()
           
            if isSuccess == true {
                self?.temprature = data.main?.temp ?? 0.0
                onSuccess()
            } else {
                onError(AppConfig.Alert.SomethingWrong)
            }
        },error:{ statusCode, data, message in
            KRProgressHUD.dismiss()
            onError(AppConfig.Alert.SomethingWrong)
        }, failure: { error in
            KRProgressHUD.dismiss()
            onError(error.localizedDescription)
        },completion: {
        })
    }
    
}

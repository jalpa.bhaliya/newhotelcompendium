//
//  HomeVC.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 28/09/23.
//

import UIKit
import AdvancedPageControl
import SDWebImage

class HomeVC: UIViewController {

    @IBOutlet weak var collectionviewDetail: UICollectionView!
    
    @IBOutlet weak var consCollectionviewDetailHeight: NSLayoutConstraint!
    @IBOutlet weak var tblSidemenu: UITableView!
    @IBOutlet var mainViewController: UIView!
    @IBOutlet var presentViewContainer: UIView!
    @IBOutlet weak var collectionviewHotelCategories: UICollectionView!
    var vmHotelList = HotelListVM()
    var vmtemp = TempVM()
    var vmHotelCategory = HotelCategoryVM()
    var time = ""
    var date = ""
    var heightCons = 0.0
    var hotelMenu_ID = ""
    
    var arrTitle = ["Home","Arrival Info","Dining Info","Hotel Info","Local Info"]
    var arrTitleSidemenu = ["Compodiam Home","Search Hotel Facilities","Arrival Information","Dinning Information","Location Information","Open in Google Map","My Notifications","Search Compodiam","Book This Compodiam","Download PDF","Share with friends"]
    var selectedIndex = 0
  
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionviewSetup()
        setUpSidemenu()
        getHotelList()
        printTimestamp()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        heightCons = collectionviewDetail.collectionViewLayout.collectionViewContentSize.height
        consCollectionviewDetailHeight.constant = heightCons
        self.view.layoutIfNeeded()
    }
    func collectionviewSetup(){
        collectionviewHotelCategories.delegate = self
        collectionviewHotelCategories.dataSource = self
        collectionviewHotelCategories.register(UINib(nibName: "HotelCategoriesCollectionviewCell", bundle: nil), forCellWithReuseIdentifier: "HotelCategoriesCollectionviewCell")
        
        collectionviewDetail.delegate = self
        collectionviewDetail.dataSource = self
        collectionviewDetail.register(UINib(nibName: "HomeDetailCollectionviewCell", bundle: nil), forCellWithReuseIdentifier: "HomeDetailCollectionviewCell")
        collectionviewDetail.register(UINib(nibName: "CommonCollectionviewCell", bundle: nil), forCellWithReuseIdentifier: "CommonCollectionviewCell")
        
    }
    func getHotelList(){
        vmHotelList.getHotelList(fiHotelsId: "\(100)") {
            DispatchQueue.main.async {
                self.collectionviewHotelCategories.reloadData()
                self.getTemp()
            }
        } onError: { error in
            print(error)
        }

    }
    func getTemp(){
        let lat = vmHotelList.homeDetail?[0].fiLatitude ?? ""
        let lon = vmHotelList.homeDetail?[0].fiLongitude ?? ""
        
        vmtemp.getTemprature(lat: lat, lon: lon, appid: "bdadd086fe6101fbf6105f399715edf2", units: "Metric") {
            self.collectionviewDetail.reloadData()
        } onError: { error in
            print(error)
        }

    }
    func printTimestamp() {
        let timestamp = DateFormatter.localizedString(from: NSDate() as Date, dateStyle: .long, timeStyle: .short)
        print(timestamp)
        let array = timestamp.components(separatedBy: "at")
        _ = array[1]
        let Date = array[0]
        let array_new = Date.components(separatedBy: " ")
        date = "\(array_new[0]) \(array_new[1]), \(array_new[2])"
    }
    func setUpSidemenu(){
        tblSidemenu.delegate = self
        tblSidemenu.dataSource = self
        tblSidemenu.register(UINib(nibName: "SidemenuTblCell", bundle: nil), forCellReuseIdentifier: "SidemenuTblCell")
    }
    @IBAction func btnSearch(_ sender: UIButton) {
        presentViewContainer.frame = mainViewController.frame
        mainViewController.addSubview(presentViewContainer)
    }
    
    @IBAction func btnBackToHome(_ sender: UIButton) {
        presentViewContainer.removeFromSuperview()
    }
    
}
//MARK: - Collectionview
extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionviewHotelCategories{
            return vmHotelList.hotelListData.count
        }
        else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionviewHotelCategories{
            let cell = collectionviewHotelCategories.dequeueReusableCell(withReuseIdentifier: "HotelCategoriesCollectionviewCell", for: indexPath) as! HotelCategoriesCollectionviewCell
            cell.lblName.text = vmHotelList.hotelListData[indexPath.row].hotelName
            cell.imgHotelLogo.sd_setImage(with: URL(string: vmHotelList.hotelListData[indexPath.row].img), placeholderImage: UIImage(named: "home"))
            if selectedIndex == indexPath.row{
                cell.setCornerRadius()
            }else{
                cell.setDefault()
            }
            return cell
        }else{
            if selectedIndex == 0{
                let cell = collectionviewDetail.dequeueReusableCell(withReuseIdentifier: "HomeDetailCollectionviewCell", for: indexPath) as! HomeDetailCollectionviewCell
                guard let data = vmHotelList.homeDetail else {
                    return cell
                }
                cell.lblDate.text = date
                cell.lblTemp.text = "\(vmtemp.temprature ?? 0.0)°C"
                cell.configure(item: data[indexPath.row])
                return cell
            }else{
                let cell = collectionviewDetail.dequeueReusableCell(withReuseIdentifier: "CommonCollectionviewCell", for: indexPath) as! CommonCollectionviewCell
                cell.getHotelCategory(hotelMenu_ID: hotelMenu_ID)
                return cell
            }
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionviewHotelCategories{
            return CGSize(width: 110, height: 110)
        }
        else{
            return CGSize(width: collectionviewDetail.frame.size.width, height: collectionviewDetail.frame.size.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.viewDidLayoutSubviews()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        hotelMenu_ID = vmHotelList.hotelListData[indexPath.row].hotelId
        collectionviewHotelCategories.reloadData()
        collectionviewDetail.reloadData()
    }
}
//MARK: - Tableview
extension HomeVC : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTitleSidemenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSidemenu.dequeueReusableCell(withIdentifier: "SidemenuTblCell", for: indexPath) as! SidemenuTblCell
        cell.lblTitle.text = arrTitleSidemenu[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}

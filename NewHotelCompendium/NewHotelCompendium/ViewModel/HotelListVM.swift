//
//  HotelListVM.swift
//  NewHotelCompendium
//
//  Created by Rajesh Shiyal on 03/10/23.
//

import Foundation
import KRProgressHUD
import Moya

class HotelListVM{
    var hotelList : [HotelListFoMenuList]?
    var homeDetail : [HotelListFoHotelList]?
    var hotelListData = [HotelList]()
    //MARK: - Api for get dashboard Data
    func getHotelList(fiHotelsId:String, onSuccess: @escaping () -> Void, onError: @escaping (String) -> Void) {
        KRProgressHUD.show()
      
        Network.request(.getHotelName(fiHotelsId:fiHotelsId),decodeType: HotelListRootClass.self, errorDecodeType: HotelListRootClass.self, decoder: JSONDecoder(), success: { [weak self] isSuccess , data in
           
            KRProgressHUD.dismiss()
           
            if isSuccess == true {
                self?.hotelListData = []
                self?.hotelListData.append(HotelList(hotelId: "", hotelName: "Home", img: ""))
                
                for i in data.foMenuList ?? []{
                    self?.hotelListData.append(HotelList(hotelId: i.fiMenuId ?? "", hotelName: i.fsMenuName ?? "", img: i.fsImage ?? ""))
                }
                print(self?.hotelListData)
                //self?.hotelList = data.foMenuList ?? []
                self?.homeDetail = data.foHotelList ?? []
                onSuccess()
            } else {
                onError(AppConfig.Alert.SomethingWrong)
            }
        },error:{ statusCode, data, message in
            KRProgressHUD.dismiss()
            onError(AppConfig.Alert.SomethingWrong)
        }, failure: { error in
            KRProgressHUD.dismiss()
            onError(error.localizedDescription)
        },completion: {
        })
    }
    
}

struct HotelList{
    let hotelId:String
    let hotelName:String
    let img:String
}
